// Initialize Firebase(2)



var config = {
    apiKey: "AIzaSyBGsS8AFrQN_NbtWuV-Hjq6WFWrMrOZtyc",
    authDomain: "medco-a07a1.firebaseapp.com",
    databaseURL: "https://medco-a07a1.firebaseio.com",
    projectId: "medco-a07a1",
    storageBucket: "medco-a07a1.appspot.com",
    messagingSenderId: "329933879692"
  };
  firebase.initializeApp(config);
  
  //Reference for form collection(3)
  let formMessage = firebase.database().ref('medco_datos');
  
  //listen for submit event//(1)
  document
    .getElementById('registrationform')
    .addEventListener('submit', formSubmit);

   
  
  //Submit form(1.2)
  function formSubmit(e) {
    e.preventDefault();
    // Get Values from the DOM
    let nombre = document.querySelector('#nombre').value;
    let email = document.querySelector('#email').value;
    let mensaje = document.querySelector('#mensaje').value;
    let sugerencia = document.querySelector('#sugerencia').value;
 
    //send message values
    sendMessage(nombre, email, mensaje, sugerencia);
  
    //Show Alert Message(5)
    document.querySelector('.alert').style.display = 'block';
  
    //Hide Alert Message After Seven Seconds(6)
    setTimeout(function() {
      document.querySelector('.alert').style.display = 'none';
    }, 7000);
  



    //Form Reset After Submission(7)
    document.getElementById('registrationform').reset();
  }

  
  
  //Send Message to Firebase(4)
  
  function sendMessage(nombre, email, mensaje, sugerencia) {
    let newFormMessage = formMessage.push();
    newFormMessage.set({
        nombre: nombre  ,
      email: email,
      mensaje: mensaje,
      sugerencia: sugerencia
      
    });
  }